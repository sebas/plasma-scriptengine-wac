/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef WEBKITBRIDGE_H
#define WEBKITBRIDGE_H

#include <QObject>
#include <QString>

#include "webpage.h"


namespace WAC {

class WebKitBridgePrivate;

class WebKitBridge : public QObject
{
Q_OBJECT
    Q_ENUMS(Permission)

public:
    enum Permission {
        Denied = 0,                     //!< Access to the call is denied
        Allowed = 1,                    //!< Access to the call is allowed
        Ignored = 2                     //!< Access to the call is not allowed,
                                        //!  the callee will not notice this however
                                        //!  and will be served fake or empty data
                                        //! This is useful to make applications run,
                                        //! even if we don't trust them, we expect them
                                        //! to handle invalid data gracefully in this case
    };

    WebKitBridge(QObject* parent = 0);
    ~WebKitBridge();

    virtual void init();

    Q_PROPERTY(QStringList consoleLog READ consoleLog NOTIFY consoleLogChanged);

    Q_INVOKABLE bool isAuthorized(const QString &domain); // check if a feature's available
    Q_INVOKABLE bool isIgnored(const QString &domain); // check if a call should return bogus data


    WebKitBridge::Permission permission(const QString &domain);
    void setPermission(const QString &domain, Permission permission); // must NOT be INVOKABLE from the runtime!

    QStringList consoleLog();
    void setConsoleLog(const QStringList &log);

    void appendConsoleLog(const QString &logline);

    void setPage(Plasma::WebPage *p);

Q_SIGNALS:
    void consoleLogChanged(const QStringList);
    void alert(const QString &message);

private Q_SLOTS:
    void consoleMessage(const QString &message, int lineNumber, const QString &sourceID);

private:
    WebKitBridgePrivate* d;
};

} // namespace WAC

#endif
