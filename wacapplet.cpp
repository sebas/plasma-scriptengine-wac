/*
Copyright (c) 2007 Zack Rusin <zack@kde.org>
Copyright (c) 2008 Beat Wolf <asraniel@fryx.ch>
Copyright (C) 2011 Marco Martin <mart@kde.org>
Copyright 2012 Sebastian Kügler <sebas@kde.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */
#include "wacapplet.h"

#include "deviceapis.h"
#include "configxml.h"

#include <QDeclarativeComponent>
#include <QDeclarativeEngine>
#include <QDeclarativeContext>
#include <QDeclarativeView>
#include <QByteArray>
#include <QFile>
#include <QWebFrame>
#include <QXmlStreamReader>

#include <KGlobal>
#include <KStandardDirs>

#include <Plasma/Applet>
#include <Plasma/DeclarativeWidget>
#include <Plasma/Package>
#include <Plasma/WebView>

#include "dashboardjs.h"


class WacAppletPrivate {

public:
    WacAppletPrivate()
    {
        deviceApis = 0;
        configXml = 0;
    }
    WAC::DeviceApis*deviceApis;
    WAC::ConfigXml* configXml;
};



WacApplet::WacApplet(QObject *parent, const QVariantList &args)
    : WebApplet(parent, args),
    d(new WacAppletPrivate())
{
}

WacApplet::~WacApplet()
{
}

void WacApplet::overlayLoadFinished(QDeclarativeComponent::Status s)
{
    if (s == QDeclarativeComponent::Ready) {
        kDebug() << "Setting configXml context property";
        //d->declarativeView->mainComponent()->creationContext()->setContextProperty("consoleLog", "Our log ...");
        QDeclarativeContext *con = overlay()->engine()->rootContext();
        if (con) {
            con->setContextProperty("configXml", d->configXml);
        } else {
            kDebug() << "No context :(";
        }
    }
    WebApplet::overlayLoadFinished(s);
}

QStringList WacApplet::requiredExtensions() const
{
    return d->configXml->requiredExtensions();
}

bool WacApplet::init()
{
    //applet()->setAspectRatioMode(Plasma::FixedSize);
    parseConfigXml();

    return WebApplet::init();
    applet()->setBackgroundHints(Plasma::Applet::StandardBackground);
}

void WacApplet::loadFinished(bool success)
{
    WebApplet::loadFinished(success);
    if (success) {
        applet()->setAspectRatioMode(Plasma::IgnoreAspectRatio);
        view()->resize(applet()->geometry().size());
        overlay()->resize(applet()->geometry().size());
    }
}

void WacApplet::constraintsEvent(Plasma::Constraints constraints)
{
    if (constraints & Plasma::FormFactorConstraint) {
       // applet()->setBackgroundHints(Plasma::Applet::NoBackground);
    }
}


void WacApplet::initJsObjects()
{
    kDebug() << "Initializing JSOBJECTs!!!";

    view()->mainFrame()->addToJavaScriptWindowObject("configxmlinternal", d->configXml);
    //overlay()->engine()->rootContext();

    d->deviceApis = new WAC::DeviceApis();
    d->deviceApis->init();
    view()->mainFrame()->addToJavaScriptWindowObject("deviceapisinternal", d->deviceApis);
    QString jsapiPath =  KStandardDirs::locate("data", "plasma/wac/devicestatus.js");
//     kDebug() << "jsapiPath is " << jsapiPath;
    QByteArray jsapi;
    QFile f(jsapiPath);
    //f.open(QIODevice::ReadOnly);
    if (!f.open(QIODevice::ReadOnly)) {
        kWarning() << "Couldn't open info file: " << jsapiPath;
        return;
    }
    jsapi = f.readAll();

//     kDebug() << "jsapi :: " << jsapi;
    view()->mainFrame()->evaluateJavaScript(jsapi);
}

bool WacApplet::parseConfigXml()
{
    if (!d->configXml) {
        d->configXml = new WAC::ConfigXml(this);
    }
    QString p = package()->path() + "config.xml";
    QFile f(p);
    if (!f.open(QIODevice::ReadOnly)) {
        kWarning() << "Couldn't open info file: " << p ;
        return false;
    }

    QMap<QString, QString> infoMap;
    QString str = f.readAll();
    f.close();
    QXmlStreamReader reader(str);
    QStringList requiredFeatures;
    QString appName;
    QString appDescription;

    while (!reader.atEnd()) {
        reader.readNext();
        // do processing
        if (reader.isStartElement()) {
            const QString n = reader.name().toString();
            if (n == "feature") {
                if (reader.attributes().value("required").toString() == "true") {
                    kDebug() << "ReqFea: " << reader.attributes().value("name").toString() << requiredFeatures;
                    requiredFeatures << reader.attributes().value("name").toString();
                }
            } else if (n == "name") {
                appName = reader.attributes().value("short").toString();
                appDescription = reader.readElementText().trimmed();
            }
        }
    }
    d->configXml->setAppName(appName);
    d->configXml->setRequiredFeatures(requiredFeatures);
//     kDebug() << "!!!! required features: " << requiredFeatures;
    return true;
}


#include "wacapplet.moc"
