/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "scriptapi.h"

#include <QStringList>

#include <KDebug>

namespace WAC {

class ScriptApiPrivate {

public:
    ScriptApiPrivate()
    {
        domainPermissions["FileDialog"] = ScriptApi::Allowed;
        domainPermissions["HTTP"] = ScriptApi::Allowed;
        domainPermissions["FileSystem"] = ScriptApi::Allowed;
        domainPermissions["NetworkStatus"] = ScriptApi::Allowed;
        domainPermissions["NetworkManagement"] = ScriptApi::Allowed;
        domainPermissions["SystemInformation"] = ScriptApi::Allowed;
        domainPermissions["Battery"] = ScriptApi::Allowed;
        domainPermissions["Geolocation"] = ScriptApi::Allowed;
        domainPermissions["Contacts"] = ScriptApi::Allowed;
        domainPermissions["Calendar"] = ScriptApi::Allowed;
        domainPermissions["Display"] = ScriptApi::Allowed;
    }
    QMap<QString, ScriptApi::Permission> domainPermissions;
};


ScriptApi::ScriptApi(QObject* parent)
    : QObject(parent),
      d(new ScriptApiPrivate)
{
    setObjectName(QLatin1String("scriptapi"));
}

ScriptApi::~ScriptApi()
{
}

void ScriptApi::init()
{
}

void ScriptApi::setPermission(const QString &domain, Permission permission)
{
    if (d->domainPermissions.contains(domain)) {
        kWarning() << "Permission domain \"" << domain << "\" does not exist, choose one of " << d->domainPermissions.keys();
        return;
    }
    d->domainPermissions[domain] = permission;
}

ScriptApi::Permission ScriptApi::permission(const QString &domain)
{
    if (d->domainPermissions.contains(domain)) {
        kWarning() << "Permission domain \"" << domain << "\" does not exist, choose one of " << d->domainPermissions.keys();
        return ScriptApi::Denied;
    }
    return d->domainPermissions.value(domain);
}

bool ScriptApi::isAuthorized(const QString &domain)
{
    if (d->domainPermissions.contains(domain)) {
        return (d->domainPermissions.value(domain) == ScriptApi::Allowed);
    }
    kWarning() << "Permission domain \"" << domain << "\" does not exist, choose one of " << d->domainPermissions.keys();
    return false;
}

bool ScriptApi::isIgnored(const QString &domain)
{
    if (d->domainPermissions.contains(domain)) {
        return (d->domainPermissions.value(domain) == ScriptApi::Ignored);
    }
    kWarning() << "Permission domain \"" << domain << "\" does not exist, choose one of " << d->domainPermissions.keys();
    return false;
}

} // namespace
#include "scriptapi.moc"
