/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DEVICEAPIS_H
#define DEVICEAPIS_H

#include "scriptapi.h"

#include <QObject>
#include <KUrl>


namespace WAC {

class DeviceApisPrivate;

class DeviceApis : public ScriptApi
{
Q_OBJECT

public:

    DeviceApis(QObject* parent = 0);
    ~DeviceApis();

    virtual void init();

private:
    DeviceApisPrivate* d;
};

} // namespace WAC

#endif

