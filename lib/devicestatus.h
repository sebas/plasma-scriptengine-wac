/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef DEVICESTATUS_H
#define DEVICESTATUS_H

#include "scriptapi.h"

#include <QObject>
#include <QVariant>

namespace WAC {

class DeviceStatusPrivate;

class DeviceStatus : public ScriptApi
{
Q_OBJECT

public:
    Q_PROPERTY(unsigned short resolutionHeight READ resolutionHeight);
    Q_PROPERTY(unsigned short batteryLevel READ batteryLevel NOTIFY batteryLevelChanged);
    Q_PROPERTY(bool batteryBeingCharged READ batteryBeingCharged  NOTIFY batteryBeingChargedChanged);

    DeviceStatus(QObject* parent = 0);
    ~DeviceStatus();

    Q_INVOKABLE QVariantList getComponents(const QString &aspect);
    Q_INVOKABLE QVariant value(const QString &aspect, const QString &prop);
    void setBatteryLevel(unsigned short level);
    unsigned short batteryLevel();
    bool batteryBeingCharged();
    unsigned short resolutionHeight();

public Q_SLOTS:
    void timeout();

Q_SIGNALS:
    void batteryLevelChanged(unsigned short level);
    void batteryBeingChargedChanged(bool);

private Q_SLOTS:
    void connectBattery();
    void updateBatteryChargePercent(int newValue, const QString &udi);
    void updateBatteryChargeState(int newState, const QString &udi);

private:
    DeviceStatusPrivate* d;
};

} // namespace WAC

#endif

