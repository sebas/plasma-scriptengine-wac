/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef CONFIGXML_H
#define CONFIGXML_H

#include <QObject>
#include <QString>

namespace Plasma {
    class Package;
}

namespace WAC {

class ConfigXmlPrivate;

class ConfigXml : public QObject
{
Q_OBJECT

public:
    Q_PROPERTY(QString appName READ appName NOTIFY appNameChanged);
    Q_PROPERTY(QStringList requiredFeatures READ requiredFeatures NOTIFY requiredFeaturesChanged);
    Q_PROPERTY(QStringList requiredExtensions READ requiredExtensions  NOTIFY requiredExtensionsChanged);

    ConfigXml(QObject* parent = 0);
    ~ConfigXml();

    QStringList requiredFeatures();
    QStringList requiredExtensions();
    QString appName() const;

    void setRequiredFeatures(const QStringList &rf);
    void setAppName(const QString &n);

Q_SIGNALS:
    void appNameChanged(const QString &appName);
    void requiredFeaturesChanged(const QStringList &rf);
    void requiredExtensionsChanged(const QStringList &re);

private:
    ConfigXmlPrivate* d;
};

} // namespace WAC

#endif

