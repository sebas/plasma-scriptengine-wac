/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "configxml.h"

#include <QTimer>
#include <QStringList>

#include <Plasma/Package>

#include <KDebug>

namespace WAC {

class ConfigXmlPrivate {

public:
    ConfigXmlPrivate()
    {

        wacExtensions["all"] = QStringList() << "Battery"
                                            << "SystemInformation"
                                            << "Display"
                                            << "FileDialog"
                                            << "FileSystem"
                                            << "NetworkStatus"
                                            << "NetworkManagement"
                                            << "Geolocation"
                                            << "Contacts"
                                            << "Calendar"
                                            << "HTTP";
        wacExtensions["http://wacapps.net/api/devicestatus"] = QStringList() << "Battery"
                                                                             << "SystemInformation"
                                                                             << "Display"
                                                                             << "NetworkStatus";
    }
    QStringList extensionsFromWac(const QString &wacFeature) const;
    QStringList requiredFeatures;
    QStringList requiredExtensions;
    QMap<QString, QStringList> wacExtensions;
    QString appName;
};


ConfigXml::ConfigXml(QObject* parent)
    : QObject(parent),
      d(new ConfigXmlPrivate)
{
    setObjectName(QLatin1String("configxml"));
}

ConfigXml::~ConfigXml() {}

QStringList ConfigXml::requiredFeatures()
{
    return d->requiredFeatures;
}

void ConfigXml::setAppName(const QString &n)
{
    if (d->appName != n) {
        d->appName = n;
        kDebug() << "Set appname: " << d->appName;
        emit appNameChanged(d->appName);
    }
}

QString ConfigXml::appName() const
{
    return d->appName;
}

void ConfigXml::setRequiredFeatures(const QStringList &rf)
{
    d->requiredFeatures = rf;
    d->requiredExtensions.clear();
    foreach (const QString &f, rf) {
        QStringList es = d->extensionsFromWac(f);
        foreach (const QString &e, es) {
            if (!d->requiredExtensions.contains(e)) {
                d->requiredExtensions << e;
            }
        }
    }
    kDebug() << "RFFFF:: " << rf << d->requiredExtensions;
    emit requiredFeaturesChanged(rf);
    emit requiredExtensionsChanged(d->requiredExtensions);
}

QStringList ConfigXml::requiredExtensions()
{
    return d->requiredExtensions;
}

QStringList ConfigXmlPrivate::extensionsFromWac(const QString &wacFeature) const
{
    return wacExtensions[wacFeature];
}


} // namespace
#include "configxml.moc"
