/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef SCRIPTAPI_H
#define SCRIPTAPI_H

#include <QObject>
#include <QString>


namespace WAC {

class ScriptApiPrivate;

class ScriptApi : public QObject
{
Q_OBJECT
    Q_ENUMS(Permission)

public:
    enum Permission {
        Denied = 0,                     //!< Access to the call is denied
        Allowed = 1,                    //!< Access to the call is allowed
        Ignored = 2                     //!< Access to the call is not allowed,
                                        //!  the callee will not notice this however
                                        //!  and will be served fake or empty data
                                        //! This is useful to make applications run,
                                        //! even if we don't trust them, we expect them
                                        //! to handle invalid data gracefully in this case
    };

    ScriptApi(QObject* parent = 0);
    ~ScriptApi();

    virtual void init();

    Q_INVOKABLE bool isAuthorized(const QString &domain); // check if a feature's available
    Q_INVOKABLE bool isIgnored(const QString &domain); // check if a call should return bogus data

    ScriptApi::Permission permission(const QString &domain);
    void setPermission(const QString &domain, Permission permission); // must NOT be INVOKABLE from the runtime!

private:
    ScriptApiPrivate* d;
};

} // namespace WAC

#endif
