/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "devicestatus.h"
#include "scriptapi.h"

#include <QApplication>
#include <QColormap>
#include <QDesktopWidget>
#include <QFile>
#include <QTimer>
#include <QVariant>

#include <KDebug>
#include <KGlobal>
#include <KLocale>

#include <solid/device.h>
#include <solid/battery.h>

namespace WAC {

class DeviceStatusPrivate {

public:
    DeviceStatusPrivate()
    {
        batteryLevel = 10;
        batteryBeingCharged = false;
        timer = 0;
        offset = -1;
    }
    ScriptApi* q;

    unsigned short batteryLevel;
    bool batteryBeingCharged;
    int offset;
    QTimer *timer;

    QVariant battery(const QString &prop);
    QVariant device(const QString &prop);
    QVariant display(const QString &prop);
    QVariant memoryUnit(const QString &prop);
    QVariant operatingSystem(const QString &prop);
    QVariant webRuntime(const QString &prop);
    QVariant wifiHardware(const QString &prop);
    QVariant wifiNetwork(const QString &prop);
};


DeviceStatus::DeviceStatus(QObject* parent)
    : ScriptApi(parent),
      d(new DeviceStatusPrivate)
{
    d->q = this;
    setObjectName(QLatin1String("devicestatus"));
    d->timer = new QTimer(this);
    d->timer->setInterval(3000);
    d->timer->start();
    timeout();
    connect(d->timer, SIGNAL(timeout()), SLOT(timeout()));
    connectBattery();
}

DeviceStatus::~DeviceStatus() {}

unsigned short DeviceStatus::resolutionHeight()
{
    return 1080;
}

void DeviceStatus::timeout()
{
    return;
    if (d->batteryLevel > 99) {
        d->offset = -1;
    } else if (d->batteryLevel == 0) {
        d->offset = 1;
    }
    d->batteryLevel = d->batteryLevel+d->offset;
    //kDebug() << "emit batteryLevelChanged() " << d->batteryLevel << " " << d->offset ;
    emit batteryLevelChanged(d->batteryLevel);
}

unsigned short DeviceStatus::batteryLevel()
{
    return d->batteryLevel;
}

bool DeviceStatus::batteryBeingCharged()
{
    return d->batteryBeingCharged;
}

QVariantList DeviceStatus::getComponents(const QString &aspect)
{
    QVariantList l;
    l << "Eins" << "Due" << "Tres" << aspect;
    return l;
}

QVariant DeviceStatus::value(const QString &aspect, const QString &prop)
{
    if (aspect == "Battery") {
        return d->battery(prop);
    } else if (aspect == "Device") {

    } else if (aspect == "Display") {
        return d->display(prop);
    } else if (aspect == "MemoryUnit") {
        return d->memoryUnit(prop);
    } else if (aspect == "OperatingSystem") {
        return d->operatingSystem(prop);
    } else if (aspect == "WebRuntime") {

    } else if (aspect == "WifiHardware") {

    } else if (aspect == "WifiNetwork") {

    }

    kWarning() << "No handler for aspect: " << aspect;
    return QVariant();
}

QVariant DeviceStatusPrivate::battery(const QString& prop)
{
    if (q->isIgnored("Battery")) {
        return QVariant();
    }
    if (prop == "batteryLevel") {
        return batteryLevel;
    } else if (prop == "batteryBeingCharged") {
        return batteryBeingCharged;
    }
    return QVariant();
}

void DeviceStatus::connectBattery()
{
    const QList<Solid::Device> listBattery =
                        Solid::Device::listFromType(Solid::DeviceInterface::Battery, QString());
    foreach (const Solid::Device &deviceBattery, listBattery) {
        const Solid::Battery* battery = deviceBattery.as<Solid::Battery>();

        if (battery && (battery->type() == Solid::Battery::PrimaryBattery)) {
            d->batteryLevel = battery->chargePercent();
            emit batteryLevelChanged(d->batteryLevel);
            d->batteryBeingCharged= battery->chargeState() == Solid::Battery::Charging;
            emit batteryBeingChargedChanged(d->batteryBeingCharged);
            kDebug() << "Charging, level: " << d->batteryLevel << d->batteryBeingCharged;

            connect(battery, SIGNAL(chargeStateChanged(int,QString)), this,
                    SLOT(updateBatteryChargeState(int,QString)));
            connect(battery, SIGNAL(chargePercentChanged(int,QString)), this,
                    SLOT(updateBatteryChargePercent(int,QString)));
        }
    }
}

void DeviceStatus::updateBatteryChargeState(int newState, const QString& udi)
{
    Q_UNUSED(udi);
    bool state;
    if (newState == Solid::Battery::NoCharge) {
        state = false;
    } else if (newState == Solid::Battery::Charging) {
        state = true;
    } else if (newState == Solid::Battery::Discharging) {
        state = false;
    }
    d->batteryBeingCharged = state;
//     kDebug() << "Charging, level: " << state << d->batteryLevel << d->batteryBeingCharged;
    emit batteryBeingChargedChanged(state);
}

void DeviceStatus::updateBatteryChargePercent(int newValue, const QString& udi)
{
    Q_UNUSED(udi);
//     kDebug() << "Charging, level: " << d->batteryLevel << d->batteryBeingCharged;
    d->batteryLevel = newValue;
    emit batteryLevelChanged(newValue);
}

QVariant DeviceStatusPrivate::device(const QString& prop)
{
    if (q->isIgnored("SystemInformation")) {
        return QVariant();
    }
    if (prop == "imei") {
        return QString();
    } else if (prop == "model") {

    } else if (prop == "vendor") {

    } else if (prop == "version") {

    }
    return QVariant();
}

QVariant DeviceStatusPrivate::display(const QString& prop)
{
    if (q->isIgnored("Display")) {
        return QVariant();
    }
    if (prop == "resolutionHeight") {
        return QApplication::desktop()->screenGeometry().height();
    } else if (prop == "resolutionWidth") {
        return QApplication::desktop()->screenGeometry().width();
    } else if (prop == "pixelAspectRatio") {
        return (qreal)QApplication::desktop()->screenGeometry().width() /
               (qreal)QApplication::desktop()->screenGeometry().height();
    } else if (prop == "dpiX") {
        return QApplication::desktop()->physicalDpiX();
    } else if (prop == "dpiY") {
        return QApplication::desktop()->physicalDpiY();
    } else if (prop == "colorDepth") {
        return QColormap::instance().depth();
    }
    return QVariant();
}

QVariant DeviceStatusPrivate::memoryUnit(const QString& prop)
{
    qulonglong size;
    qulonglong availableSize;
//     return 0;
    if (QFile::exists("/proc/meminfo")) {
        QFile file("/proc/meminfo");
        if (file.open(QIODevice::ReadOnly)) {
//                 char buf[512];
//                 while (file.readLine(buf, sizeof(buf) - 1) > 0) {
//                         if (strncmp(buf,"MemTotal:",9)==0) {
//                                 unsigned long v;
//                                 v = strtoul(&buf[9],NULL,10);
//                                 size = v * 1024; // Total RAM
//                                 //break;
//                         }
//                         if (strncmp(buf,"MemFree:",8)==0) {
//                                 unsigned long v;
//                                 v = strtoul(&buf[8],NULL,10);
//                                 availableSize = v * 1024; // Cached memory in RAM
//                                 //break;
//                         }
//                 }
                QStringList meminfo = QString(file.readAll()).split('\n');
                // FIXME: bogus info returned for values :/
                foreach (const QString &line, meminfo) {
                    int unit = 1024;
                    if (line.endsWith("kB")) unit = 1024;
                    if (line.endsWith("MB")) unit = 1024*1024;
                    if (line.startsWith("MemTotal:")) {
                        QStringList lp = line.split(":")[1].trimmed().split(' ');
                        size = lp[0].toULong() * unit;
                        //kDebug() << " lp: " << lp << size << unit << lp[0].toUInt() << (lp[0].toUInt()*1024) << (5787784*unit);
                    } else if (line.startsWith("MemFree:")) {
                        availableSize = line.split(":")[1].trimmed().split(' ')[0].toULong() * unit;
                    }
                }
                file.close();
                
        }
    }
//     kDebug() << "Memory: " << availableSize << " / " << size;
    if (q->isIgnored("SystemInformation")) {
        return QVariant();
    }
    if (prop == "size") {
        return size;
    } else if (prop == "availableSize") {
        return availableSize;
    }
    return QVariant();
}


QVariant DeviceStatusPrivate::operatingSystem(const QString& prop)
{
    if (!q->isAuthorized("SystemInformation")) {
        return QVariant();
    }
    if (prop == "language") {
        return KGlobal::locale()->language();
    } else if (prop == "version") {
        return "4.8.90"; // FIXME: KDE_VERSION_STRING ??
    } else if (prop == "name") {
        return "KDE Plasma";
    } else if (prop == "vendor") {
        return "KDE";
    }
    return QVariant();
}

QVariant DeviceStatusPrivate::webRuntime(const QString& prop)
{
    if (prop == "") {

    } else if (prop == "") {

    }
    return QVariant();
}

QVariant DeviceStatusPrivate::wifiHardware(const QString& prop)
{
    if (prop == "") {

    } else if (prop == "") {

    }
    return QVariant();
}

QVariant DeviceStatusPrivate::wifiNetwork(const QString& prop)
{
    if (prop == "") {

    } else if (prop == "") {

    }
    return QVariant();
}


} // namespace
#include "devicestatus.moc"
