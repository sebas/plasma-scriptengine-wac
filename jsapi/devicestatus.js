/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// enum SwitchType {"ON", "OFF"};
// enum NetworkStates {"connected", "available", "forbidden"};

function DeviceStatus() {
    this.getPropertyValue = function(cb, ecb, obj) {
        var a = obj.aspect.trim();
        var p = obj.property.trim();
//         console.log(" obj.aspect requested: " + obj.aspect);
        // Battery
        // Device
        // Display
        // MemoryUnit
        // OperatingSystem
        // WebRuntime

        if (obj.aspect == "Battery" && obj.property == "batteryLevel") {
            cb(deviceapisinternal.devicestatus.batteryLevel);
            deviceapisinternal.devicestatus.batteryLevelChanged.connect(cb);
            return;
        }

        if (obj.aspect == "Battery" && obj.property == "batteryBeingCharged") {
            cb(deviceapisinternal.devicestatus.batteryBeingCharged);
            deviceapisinternal.devicestatus.batteryBeingChargedChanged.connect(cb);
            return;
        }

//         if (a == "OperatingSystem") {
//             if (p == "name") {
//                 cb("Plasma Active");
//             } else if (p == "vendor") {
//                 cb("KDE");
//             } else if (p == "version") {
//                 cb("2.4");
//             }
//             return;
//         }

        if (a == "Device") {
            if (p == "model") {
                cb("Vivaldi");
            } else if (p == "vendor") {
                cb("Plasma Team");
            } else if (p == "version") {
                cb("2.4");
            }
            return;
        }

        if (a == "WebRuntime") {
            if (p == "name") {
                cb("Plasma WAC ScriptEngine");
            } else if (p == "version") {
                cb("0.2");
            }
            return;
        }

        if (a == "Display") {
//             if (p == "name") {
//                 cb("Plasma WAC ScriptEngine");
//             } else if (p == "version") {
//                 cb("0.2");
//             }
//             return;
        }

        if (a == "MemoryUnit") {
//             if (p == "name") {
//                 cb("Plasma WAC ScriptEngine");
//             } else if (p == "version") {
//                 cb("0.2");
//             }
//             return;
        }


        //console.log("devicestatus.js: Unhandled aspect/property: " + obj.aspect + "/" + obj.property);
        cb(deviceapisinternal.devicestatus.value(a, p));
        return;
    }
}

function DeviceApis() {
    this.devicestatus = new DeviceStatus();
    this.allAvailableFeatures =  new Array("http://wacapps.net/api/devicestatus");

    this.listAvailableFeatures = function() {
        var reqFeatures = new Array();
        for (i=0; i< this.allAvailableFeatures.length; i++) {
            var feat = this.allAvailableFeatures[i];
            if (configxmlinternal.requiredFeatures.indexOf(feat) > -1) {
                reqFeatures.push(feat);
            }
        }
        return reqFeatures;
    }
}

var deviceapis = new DeviceApis();