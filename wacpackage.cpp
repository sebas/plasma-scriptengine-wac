/*
Copyright (c) 2007 Zack Rusin <zack@kde.org>
Copyright (C) 2011 Marco Martin <mart@kde.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

#include "wacpackage.h"

#include <QBuffer>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QXmlStreamReader>

#include <KDebug>

#include <KIO/CopyJob>
#include <KIO/Job>

#include <Plasma/PackageMetadata>
#include <Plasma/Package>

void recursive_print(const KArchiveDirectory *dir, const QString &path)
{
    const QStringList l = dir->entries();
    QStringList::const_iterator it = l.constBegin();
    for (; it != l.end(); ++it)
    {
        const KArchiveEntry* entry = dir->entry((*it));
        printf("mode=%07o %s %s size: %lld pos: %lld %s%s isdir=%d%s",
               entry->permissions(),
               entry->user().toLatin1().constData(),
               entry->group().toLatin1().constData(),
               entry->isDirectory() ? 0 : ((KArchiveFile*)entry)->size(),
               entry->isDirectory() ? 0 : ((KArchiveFile*)entry)->position(),
               path.toLatin1().constData(),
               (*it).toLatin1().constData(), entry->isDirectory(),
               entry->symLinkTarget().isEmpty() ? "" :
               QString(" symlink: %1").arg(
                   entry->symLinkTarget()).toLatin1().constData());

        //if (!entry->isDirectory()) printf("%d",
        //     ((KArchiveFile*)entry)->size());
        printf("\n");
        if (entry->isDirectory())
            recursive_print((KArchiveDirectory *)entry, path+(*it)+'/');
    }
}


WacPackage::WacPackage(const QString &path)
    : PackageStructure(0, "WacWidget"),
      m_isValid(false),
      m_width(0), m_height(0)
{
    kDebug() << "Loading WAC Package from " << path;
    setContentsPrefix(QString());
    QFile f(path);
    f.open(QIODevice::ReadOnly);
    m_data = f.readAll();
    f.close();
    initTempDir();
    open();
}

WacPackage::WacPackage(const QByteArray &data)
    : PackageStructure(0, "WacWidget"),
      m_isValid(false),
      m_width(0),
      m_height(0)
{
    setContentsPrefix(QString());
    m_data = data;
    initTempDir();
    open();
}

WacPackage::WacPackage(QObject *parent, QVariantList args)
    : PackageStructure(parent, "WacWidget"),
      m_isValid(false),
      m_tempDir(0),
      m_width(0),
      m_height(0)
{
    Q_UNUSED(args)
    setContentsPrefix(QString());
}

WacPackage::~WacPackage()
{
    close();
}

void WacPackage::setData(const QByteArray &data)
{
    m_data = data;
    close();
    open();
}

QByteArray WacPackage::data() const
{
    return m_data;
}

bool WacPackage::open()
{
    if (!m_tempDir) {
        initTempDir();
    }

    if (m_data.isEmpty()) {
        return false;
    }

    QBuffer buffer(&m_data);
    KZip zip(&buffer);
    if (!zip.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open the bundle!");
        return false;
    }

    const KArchiveDirectory *dir = zip.directory();

    m_isValid = extractArchive(dir, QLatin1String(""));
    qDebug()<<"Dir = "<<dir->name() << m_isValid;

    if (m_isValid) {
        setPath(m_tempDir->name());
    }

    zip.close();

    return m_isValid;
}

bool WacPackage::close()
{
    bool ret = m_tempDir;
    delete m_tempDir;
    m_tempDir = 0;
    return ret;
}

bool WacPackage::extractArchive(const KArchiveDirectory *dir, const QString &path)
{
    const QStringList l = dir->entries();

    QStringList::const_iterator it;
    for (it = l.constBegin(); it != l.constEnd(); ++it) {
        const KArchiveEntry* entry = dir->entry((*it));
        QString fullPath = QString("%1/%2").arg(path).arg(*it);
        if (entry->isDirectory()) {
            QString outDir = QString("%1%2").arg(m_tempDir->name()).arg(path);
            QDir qdir(outDir);
            qdir.mkdir(*it);
            extractArchive(static_cast<const KArchiveDirectory*>(entry), fullPath);
        } else if (entry->isFile()) {
            QString outName = QString("%1%2").arg(m_tempDir->name()).arg(fullPath.remove(0, 1));
            //qDebug()<<"-------- "<<outName;
            QFile f(outName);
            if (!f.open(QIODevice::WriteOnly)) {
                qWarning("Couldn't create %s", qPrintable(outName));
                continue;
            }
            const KArchiveFile *archiveFile = static_cast<const KArchiveFile*>(entry);
            f.write(archiveFile->data());
            f.close();
        } else {
            qWarning("Unidentified entry at %s", qPrintable(fullPath));
        }
    }
    return true;
}

void WacPackage::pathChanged()
{
    //qDebug() << "path changed";
    m_isValid = extractInfo();
}

bool WacPackage::extractInfo()
{
    QString configXml = QString("%1config.xml").arg(path());
    if (QFile::exists(configXml)) {
        return parseConfigXml(configXml);
    }

    return false;
}

bool WacPackage::installPackage(const QString &archivePath, const QString &packageRoot)
{
    //kDebug() << "??????????????" << archivePath << packageRoot;
    QFile f(archivePath);
    f.open(QIODevice::ReadOnly);
    m_data = f.readAll();
    f.close();
    open();

    if (m_isValid) {
        m_tempDir->setAutoRemove(false);
        QString pluginName = "wac_" + m_name;
        //kDebug() << "valid, so going to move it in to" << pluginName;
        KIO::CopyJob* job = KIO::move(m_tempDir->name(), QString(packageRoot + "/" + pluginName), KIO::HideProgressInfo);
        m_isValid = job->exec();

        if (m_isValid) {
            //kDebug() << "still so good ... registering";
            Plasma::PackageMetadata data;
            data.setName(m_name);
            data.setDescription(m_description);
            data.setPluginName(pluginName);
            data.setImplementationApi("wac");
            Plasma::Package::registerPackage(data, m_iconLocation);
        }
    }

    if (!m_isValid) {
        // make sure we clean up after ourselves afterwards on failure
        m_tempDir->setAutoRemove(true);
    }

    return m_isValid;
}

bool WacPackage::parseConfigXml(const QString &loc)
{
    QFile f(loc);
    if (!f.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open info file: '%s'", qPrintable(loc));
        return false;
    }

    QMap<QString, QString> infoMap;
    QString str = f.readAll();
    f.close();
    QXmlStreamReader reader(str);
    while (!reader.atEnd()) {
        reader.readNext();
        // do processing
        if (reader.isStartElement()) {
//             qDebug() << reader.name().toString();
            if (reader.name() == "icon") {
                //infoMap.insert("icon", reader.attributes().value("src");
//                 kDebug() << path();
                m_iconLocation = QString("%1%2").arg(path()).arg(reader.attributes().value("src").toString());
            } else if (reader.name() == "content") {
                const QString src = reader.attributes().value("src").toString();
                m_htmlLocation = QString("%1%2").arg(path()).arg(src);
                addFileDefinition("mainscript", src, i18n("Main Webpage"));
            } else if (reader.name() == "name") {
                m_name = reader.attributes().value("short").toString();
                m_description = reader.readElementText().trimmed();
            }
        }
    }

   
    addDirectoryDefinition("html", "/", i18n("Root HTML directory"));

    qDebug()<<"name = "<<m_name;
    qDebug()<<"html = "<<m_htmlLocation;
    qDebug()<<"icon = "<<m_iconLocation;

    return !m_htmlLocation.isEmpty();
}

void WacPackage::initTempDir()
{
    m_tempDir = new KTempDir();
    kDebug() << "initTempDir ...";
    //make it explicit
    m_tempDir->setAutoRemove(false);
}

QString WacPackage::name() const
{
    return m_name;
}

QString WacPackage::version() const
{
    return m_version;
}

QString WacPackage::description() const
{
    return m_description;
}

int WacPackage::width() const
{
    return m_width;
}

int WacPackage::height() const
{
    return m_height;
}

QString WacPackage::htmlLocation() const
{
    return m_htmlLocation;
}

QString WacPackage::iconLocation() const
{
    return m_iconLocation;
}

bool WacPackage::isValid() const
{
    return m_isValid;
}


