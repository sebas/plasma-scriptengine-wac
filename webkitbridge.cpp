/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "webkitbridge.h"

#include <QStringList>

#include <KDebug>

namespace WAC {

class WebKitBridgePrivate {

public:
    WebKitBridgePrivate()
    {
        domainPermissions["FileDialog"] = WebKitBridge::Allowed;
        domainPermissions["HTTP"] = WebKitBridge::Allowed;
        domainPermissions["FileSystem"] = WebKitBridge::Allowed;
        domainPermissions["NetworkStatus"] = WebKitBridge::Allowed;
        domainPermissions["NetworkManagement"] = WebKitBridge::Allowed;
        domainPermissions["SystemInformation"] = WebKitBridge::Allowed;
        domainPermissions["Battery"] = WebKitBridge::Allowed;
        domainPermissions["Display"] = WebKitBridge::Allowed;
    }
    
    QMap<QString, WebKitBridge::Permission> domainPermissions;
    QStringList consoleLog;
};


WebKitBridge::WebKitBridge(QObject* parent)
    : QObject(parent),
      d(new WebKitBridgePrivate)
{
    setObjectName(QLatin1String("webkitbridge"));
}

WebKitBridge::~WebKitBridge()
{
}

void WebKitBridge::init()
{
}

void WebKitBridge::setPermission(const QString &domain, Permission permission)
{
    if (d->domainPermissions.contains(domain)) {
        kWarning() << "Permission domain \"" << domain << "\" does not exist, choose one of " << d->domainPermissions.keys();
        return;
    }
    d->domainPermissions[domain] = permission;
}

WebKitBridge::Permission WebKitBridge::permission(const QString &domain)
{
    if (d->domainPermissions.contains(domain)) {
        kWarning() << "Permission domain \"" << domain << "\" does not exist, choose one of " << d->domainPermissions.keys();
        return WebKitBridge::Denied;
    }
    return d->domainPermissions.value(domain);
}

bool WebKitBridge::isAuthorized(const QString &domain)
{
    if (d->domainPermissions.contains(domain)) {
        return (d->domainPermissions.value(domain) == WebKitBridge::Allowed);
    }
    kWarning() << "Permission domain \"" << domain << "\" does not exist, choose one of " << d->domainPermissions.keys();
    return false;
}

bool WebKitBridge::isIgnored(const QString &domain)
{
    if (d->domainPermissions.contains(domain)) {
        return (d->domainPermissions.value(domain) == WebKitBridge::Ignored);
    }
    kWarning() << "Permission domain \"" << domain << "\" does not exist, choose one of " << d->domainPermissions.keys();
    return false;
}

//
void WebKitBridge::setPage(Plasma::WebPage *p)
{
    connect(p, SIGNAL(consoleMessage(const QString&, int, const QString&)),
            this, SLOT(consoleMessage(const QString&, int, const QString&)));
    connect(p, SIGNAL(alert(const QString&)), SIGNAL(alert(const QString&)));
}


// Logging
QStringList WebKitBridge::consoleLog()
{
    return d->consoleLog;
}

void WebKitBridge::setConsoleLog(const QStringList &log)
{
    if (d->consoleLog != log) {
        d->consoleLog = log;
        emit consoleLogChanged(d->consoleLog);
    }
}

void WebKitBridge::appendConsoleLog(const QString &logline)
{
    d->consoleLog << logline;
    emit consoleLogChanged(d->consoleLog);
}

void WebKitBridge::consoleMessage(const QString& message, int lineNumber,
                                       const QString& sourceID)
{
    Q_UNUSED(sourceID)
    Q_UNUSED(lineNumber)
//     kDebug()<< "JS: "<< lineNumber<<": " << message << d->consoleLog.count();
    appendConsoleLog(QString("(%1): %2").arg(QString::number(lineNumber), message));
    return;
    //d->consoleLog.append(QString("%1: %2").arg(QString(lineNumber), message));
    //QDeclarativeContext *con = d->declarativeView->engine()->rootContext();
    //con->setContextProperty("consoleLog", d->consoleLog);
}

} // namespace
#include "webkitbridge.moc"
