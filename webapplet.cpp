/*
Copyright (c) 2007 Zack Rusin <zack@kde.org>
Copyright 2012 Sebastian Kügler <sebas@kde.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */
#include "webapplet.h"
#include "webkitbridge.h"
#include "webpage.h"

#include <QDeclarativeComponent>
#include <QDeclarativeContext>
#include <QDeclarativeEngine>
#include <QDeclarativeItem>
#include <QPainter>
#include <QTimer>
#include <QWebView>
#include <QWebFrame>
#include <QWebPage>
#include <QFile>
#include <QGraphicsLinearLayout>
#include <QGraphicsAnchorLayout>

#include <KDebug>
#include <KStandardDirs>

#include <Plasma/Applet>
#include <Plasma/DeclarativeWidget>
#include <Plasma/Package>
#include <Plasma/WebView>

using namespace Plasma;

class WebApplet::Private
{
public:
    Private()
        : webKitBridge(0),
          declarativeView(0),
          view(0)
    {
    }

    void init(WebApplet *q)
    {
        loaded = false;

        Plasma::Applet *applet = q->applet();
        applet->setAcceptsHoverEvents(true);

        view = new Plasma::WebView(applet);
        WebPage *p = new WebPage(view);
        view->setPage(p);
        //declarativeView->setPage(p);
        webKitBridge = new WAC::WebKitBridge(applet);
        webKitBridge->setPage(p);
        declarativeView = new Plasma::DeclarativeWidget(applet);
        connect(declarativeView->mainComponent(), SIGNAL(statusChanged(QDeclarativeComponent::Status)),
                q, SLOT(overlayLoadFinished(QDeclarativeComponent::Status)));
        const QString qmlPath = KStandardDirs::locate("data", "plasma/wac/Overlay.qml");

        declarativeView->setQmlPath(qmlPath);

        QGraphicsAnchorLayout* lay = new QGraphicsAnchorLayout(applet);

        lay->addCornerAnchors(view, Qt::TopLeftCorner, lay, Qt::TopLeftCorner);
        lay->addCornerAnchors(view, Qt::BottomRightCorner, lay, Qt::BottomRightCorner);
        lay->addCornerAnchors(declarativeView, Qt::TopLeftCorner, lay, Qt::TopLeftCorner);
        lay->addCornerAnchors(declarativeView, Qt::BottomRightCorner, lay, Qt::BottomRightCorner);

        QObject::connect(view, SIGNAL(loadFinished(bool)),
                         q, SLOT(loadFinished(bool)));
        QObject::connect(view->page(), SIGNAL(frameCreated(QWebFrame*)),
                         q, SLOT(connectFrame(QWebFrame*)));
        q->connectFrame(view->mainFrame());

        view->mainFrame()->setScrollBarPolicy(Qt::Horizontal, Qt::ScrollBarAlwaysOff);
        view->mainFrame()->setScrollBarPolicy(Qt::Vertical, Qt::ScrollBarAlwaysOff);

        QPalette palette = view->palette();
        palette.setColor(QPalette::Base, Qt::transparent);
        view->page()->setPalette(palette);

        // Wild hack. We need to connect to the overlay's signal after everything's loaded
        timer = new QTimer(q);
        timer->setInterval(500);
        QObject::connect(timer, SIGNAL(timeout()), q, SLOT(overlayLoadFinished()));
        timer->setSingleShot(true);
        timer->start();
    }

    WAC::WebKitBridge *webKitBridge;
    Plasma::DeclarativeWidget *declarativeView;
    Plasma::WebView *view;
    bool loaded;
    QStringList log;
    QTimer *timer;
};

WebApplet::WebApplet(QObject *parent, const QVariantList &args)
    : AppletScript(parent),
      d(new Private)
{
    Q_UNUSED(args)
}

WebApplet::~WebApplet()
{
    delete d;
}

bool WebApplet::init()
{
    d->init(this);
    return true;
}


void WebApplet::paintInterface(QPainter *painter,
                               const QStyleOptionGraphicsItem *option,
                               const QRect &contentsRect)
{
    Q_UNUSED(painter)
    Q_UNUSED(option)
    Q_UNUSED(contentsRect)
}

Plasma::DeclarativeWidget* WebApplet::overlay() const
{
    return d->declarativeView;
}

Plasma::WebView* WebApplet::view() const
{
    return d->view;
}

void WebApplet::loadFinished(bool success)
{
    d->loaded = success;
}

void WebApplet::overlayLoadFinished(QDeclarativeComponent::Status s)
{
    if (s == QDeclarativeComponent::Ready) {
        kDebug() << "Setting log context property";
        //d->declarativeView->mainComponent()->creationContext()->setContextProperty("consoleLog", "Our log ...");
        QDeclarativeContext *con = d->declarativeView->engine()->rootContext();
        if (con) {
            con->setContextProperty("webKitBridge", d->webKitBridge);
        } else {
            kDebug() << "No context :(";
        }

        QDeclarativeItem* popup = d->declarativeView->rootObject()->findChild<QDeclarativeItem*>("authorizationDialog");
        if (popup) {
            connect(popup, SIGNAL(extensionsAuthorized(const QVariant&)),
                           SLOT(extensionsAuthorized(const QVariant&)));
        } else kError() << "component not found.";
    }
    extensionsAuthorized(QStringList());
}

QStringList WebApplet::requiredExtensions() const
{
    kDebug() << "should not be called for WAC";
    return QStringList();
}


void WebApplet::extensionsAuthorized(const QVariant &v)
{
    const QStringList authorizedExtensions = v.toStringList();
    bool authorized = true;
    foreach (const QString &ext, requiredExtensions()) {
        if (!authorizedExtensions.contains(ext)) {
            authorized = false;
        }
    }

    kDebug() << "App Authorization: " << authorized;
    kDebug() << "req / auth: " << requiredExtensions() << authorizedExtensions;
    if (!authorized) { // FIXME: heuristics
        kDebug() << "App not authorized.";
        d->view->mainFrame()->setHtml(QString());
        return;
    }
    QString webpage;
    webpage = package()->filePath("mainscript");

    if (webpage.isEmpty()) {
        kDebug() << "fail! no page";
        delete d->view;
        d->view = 0;
        return;
    }

    KUrl url(package()->filePath("html"));
    kDebug() << webpage << package()->path() << url;
    d->view->mainFrame()->setHtml(dataFor(webpage), url);
    return;
}


void WebApplet::connectFrame(QWebFrame *frame)
{
    connect(frame, SIGNAL(javaScriptWindowObjectCleared()),
            this, SLOT(initJsObjects()));
}

void WebApplet::initJsObjects()
{
}

QByteArray WebApplet::dataFor(const QString &str)
{
    QFile f(str);
    f.open(QIODevice::ReadOnly);
    QByteArray data = f.readAll();
    f.close();
    return data;
}

QWebPage *WebApplet::page()
{
    return d->view ? d->view->page() : 0;
}

bool WebApplet::loaded()
{
    return d->loaded;
}

#include "webapplet.moc"
