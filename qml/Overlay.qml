/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.plasma.extras 0.1 as PlasmaExtras
import org.kde.qtextracomponents 0.1 as QtExtraComponents

Item {
    id: overlay
    objectName: "overlay"
    signal extensionsAuthorized(variant authorizedExtensions)
    property int _m: 12
    property Item dialog

    property alias showMessageOverlay: messageOverlay.visible
    property alias messageIcon: messageIconItem
    property alias messageText: messageTextLabel.text
    property alias messageTitle: messageHeadingLabel.text

    PlasmaCore.FrameSvgItem {
        id: messageOverlay
        visible: (configXml.requiredExtensions.length > 0)
        //opacity: showMessageOverlay ? 1.0 : 0
        imagePath: "dialogs/background"
        anchors { fill: parent; margins: parent.width / 8; }
        PlasmaComponents.ToolButton {
            anchors { right: parent.right; top: parent.top; topMargin: _m; rightMargin: _m }
            iconSource: "dialog-close"
            onClicked: PlasmaExtras.DisappearAnimation { targetItem: messageOverlay }
        }
        Item {
            anchors { fill: parent; margins: parent.width / 8; }
            QtExtraComponents.QIconItem {
                id: messageIconItem
                icon: "task-accepted"
                width: 64
                height: 64
                anchors { left: parent.left; top: parent.top }
            }

            PlasmaExtras.Heading {
                id: messageHeadingLabel
                level: 1
                anchors { left: messageIconItem.right; right: parent.right; top: messageIconItem.top; leftMargin: _m }
                text: i18n("Authorization")
            }
            PlasmaExtras.Paragraph {
                id: messageTextLabel
                text: "text ..."
                opacity: 0.8
                anchors { left: messageIconItem.right; right: parent.right; top: messageHeadingLabel.bottom; leftMargin: _m; rightMargin: _m; topMargin: _m; }
            }
            PlasmaComponents.ToolButton {
                anchors { right: parent.right; top: messageTextLabel.bottom; topMargin: _m }
                iconSource: "dialog-password"
                text: i18n("Permissions")
                onClicked: showAuthorizationDialog()
            }
        }
    }

    PlasmaCore.FrameSvgItem {
        //color: "lightgrey"
        id: frame
        opacity: logViewerButton.checked ? 1.0 : 0.3
        height: logViewerButton.checked ? 240 : 24+_m
        imagePath: "dialogs/background"
        enabledBorders: "TopBorder"
        anchors { bottom: parent.bottom; left: parent.left; right: parent.right; }

        Behavior on height {
            NumberAnimation { duration: 300; easing.type: Easing.OutExpo; }
        }

        ListView {
            id: logList
            model: webKitBridge.consoleLog
            anchors { fill: parent; margins: _m; bottomMargin: 0; }
            clip: true
            spacing: -8
            delegate: PlasmaComponents.Label { text: modelData }
            function updateModelIndex() {
                //print("Console log ...." + (webKitBridge.consoleLog.length - 1));
                logList.currentIndex = webKitBridge.consoleLog.length - 1
            }
            Component.onCompleted: webKitBridge.consoleLogChanged.connect(updateModelIndex)
        }

        Row {
            width: 24 + _m
            anchors { right: parent.right; bottom: parent.bottom; rightMargin: _m}

            PlasmaComponents.ToolButton {
                //property Item dialog
                iconSource: "security-high"
                width: 24
                height: 24
                //visible: frame.height > 128
                onClicked: {
                    if (!overlay.dialog)
                        overlay.dialog = dialogComponent.createObject(parent)
                    overlay.dialog.open()
                }
            }

            PlasmaComponents.ToolButton {
                id: logViewerButton
                iconSource: "utilities-log-viewer"
                checkable: true
                width: 24
                height: 24

                onCheckedChanged: checked
            }
        }
    }
    Component {
        id: dialogComponent
        AuthorizationDialog { id: authorizationDialog }
    }

    function showAuthorizationDialog() {
        if (!dialog) {
            dialog = dialogComponent.createObject(overlay)
            dialog.extensionsAuthorized.connect(extensionsAuthorized);
        }
        dialog.open();

    }

    Component.onCompleted: {
        if (!dialog) {
            dialog = dialogComponent.createObject(overlay)
            dialog.extensionsAuthorized.connect(extensionsAuthorized);
        }
        print("=== Overlay.qml completed.");
        messageTitle = i18n("Authorization required");
        messageText = i18n("The app <strong>%1</strong> requires the following extensions: <ul><li>&nbsp;&nbsp;%2</li></ul>.", configXml.appName, configXml.requiredExtensions.join("</li><li>&nbsp;&nbsp;"));
        messageIcon.icon = "task-attention";
        //showAuthorizationDialog();
    }
}