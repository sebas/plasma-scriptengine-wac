/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.plasma.extras 0.1 as PlasmaExtras
import org.kde.qtextracomponents 0.1 as QtExtraComponents

PlasmaComponents.CommonDialog {
    signal extensionsAuthorized(variant authorizedExtensions)
    id: authorizationDialog
    objectName: "authorizationDialog"
    property variant authorizedExtensions: configXml.requiredExtensions

    titleText: "CommonDialog"
    buttonTexts: ["Launch App", "Cancel"]

    content: Item {
        width: 590
        height: 500
        anchors { left: parent.left; right: parent.right; }
//         anchors.horizontalCenter: parent.horizontalCenter
//         anchors.verticalCenter: parent.verticalCenter
        Item {
            anchors.fill: parent
            anchors.margins: _m*2
            PlasmaExtras.Title {
                id: authorizeTitle
                text: i18n("App requires authorization")
                anchors { left: parent.left; right: parent.right; top: parent.top; }
            }
            PlasmaExtras.Paragraph {
                id: authorizeText
                text: i18n("The app <strong>%1</strong> requires access to the following functions to run:", configXml.appName)
                anchors { left: parent.left; right: parent.right; top: authorizeTitle.bottom;}
            }
            ListView {
                id: authorizationList
                spacing: _m
                clip: true
                interactive: contentHeight > height
                anchors { left: parent.left; right: parent.right; top: authorizeText.bottom; bottom: parent.bottom; topMargin: _m }
                model: configXml.requiredExtensions
                delegate: ExtensionDelegate { ext: modelData }

            }
        }
    }

    function allowExtension(e) {
        print(" + + + + + + +");
        print("Allowing extension " + e);
        var aes = [e];
        // only add extensions once
        for (i=0; i < authorizedExtensions.length; i++) {
            var ce = authorizedExtensions[i];
            if (aes.indexOf(ce) == -1) {
                aes.push(ce);
            }
        }
        authorizedExtensions = aes;
        print("AUTH: " + requiredExtensionsSatisfied() + " " + authorizedExtensions.length + "  " + this.authorizedExtensions.join(", "));
    }

    function denyExtension(e) {
        print(" - - - - - - -");
        print("Denying  extension " + e);
        var aes = [];
        // only add extensions once
        for (i=0; i < authorizedExtensions.length; i++) {
            var ce = authorizedExtensions[i];
            if (ce != e && aes.indexOf(ce) == -1) {
                aes.push(ce);
            }
        }
        authorizedExtensions = aes;
        print("AUTH: " + requiredExtensionsSatisfied() + " " + authorizedExtensions.length + "  " + this.authorizedExtensions.join(", "));
    }

    function requiredExtensionsSatisfied() {
        for (i=0; i < configXml.requiredExtensions.length; i++) {
//             print( " EE : " + );
            if (authorizedExtensions.indexOf(configXml.requiredExtensions[i]) == -1) {
                return false
            }
        }
        return true
    }

    onButtonClicked: {
        var ok = requiredExtensionsSatisfied();
        print(" OK : " + ok);
        if (index == 0) {
//             var l = new Array();
            print("Accepting: " + requiredExtensionsSatisfied());
            extensionsAuthorized(authorizedExtensions);
            if (ok) {
                messageTitle = i18n("App authorized");
                messageText = i18n("The app <strong>%1</strong>  is allowed to use the following extensions: <ul><li>&nbsp;&nbsp;%2</li></ul>.", configXml.appName, authorizedExtensions.join("</li><li>&nbsp;&nbsp;"));
                showMessageOverlay = true;
                messageIcon.icon = "task-complete";
            } else {
                messageTitle = i18n("Authorization for app rejected");
                messageText = i18n("The app <strong>%1</strong> is not allowed to use one or more of the following extensions: <ul><li>&nbsp;&nbsp;%2</li></ul>.", configXml.appName, configXml.requiredExtensions.join("</li><li>&nbsp;&nbsp;"));
                messageIcon.icon = "task-reject";
                showMessageOverlay = true;
            }

        } else if (index == 1) {
            print("cancel");
            //extensionsAuthorized(new Array());
        }
        showMessageOverlay = true;
        print(" icon: " + messageIcon);
    }

    Component.onCompleted: {
        print(" Required Extensions: " + configXml.requiredExtensions.join(", "));
    }
}
