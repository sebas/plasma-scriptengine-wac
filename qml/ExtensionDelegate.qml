/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */


import QtQuick 1.1
import org.kde.plasma.core 0.1 as PlasmaCore
import org.kde.plasma.components 0.1 as PlasmaComponents
import org.kde.plasma.extras 0.1 as PlasmaExtras
import org.kde.qtextracomponents 0.1 as QtExtraComponents

Item {
    height: childrenRect.height
    width: 540
    property string ext: modelData
    property string extIcon: "task-accepted"
    property string extDescription: "This is the Extension's default description."
    property string extName: "::" + ext
    property bool authorizationStatus: false

    QtExtraComponents.QIconItem {
        id: iconItem
        icon: extIcon
        width: 48
        height: width
        anchors { left: parent.left; top: parent.top }
    }

    PlasmaExtras.Heading {
        //height: 12
        id: headingLabel
        level: 3
        anchors { left: iconItem.right; top: iconItem.top; right: parent.right; leftMargin: _m }
        text: extName
    }
    PlasmaExtras.Paragraph {
        id: descriptionLabel
        text: extDescription
        opacity: 0.8
        anchors { left: iconItem.right; top: headingLabel.bottom; right: authorizationSwitch.left; leftMargin: _m; rightMargin: _m; }
    }

    PlasmaComponents.Switch {
        id: authorizationSwitch
        anchors { right: parent.right; verticalCenter: parent.verticalCenter; rightMargin: _m; }
        onCheckedChanged: {
            if (checked) {
                //print("Allowing extension " + ext);
                allowExtension(ext);
                //authorizationDialog.authorizedExtensions.push(ext);
            } else {
                //print("Denying extension " + ext);
                denyExtension(ext);
                //authorizationDialog.authorizedExtensions.pop(ext);
            }
        }
        Component.onCompleted: {
            checked = (authorizedExtensions.indexOf(ext) >= 0);
        }
    }

    
    onExtChanged: {
        if (ext == "FileDialog") {
            extIcon = "system-file-manager"
            extName = i18n("File Dialog");
            extDescription = i18n("Allow the app to open a file dialog and read directory and file names, but not the files' content.");
        } else if (ext == "HTTP") {
            extIcon = "download"
            extName = i18n("HTTP Download");
            extDescription = i18n("Allow the app to download remote files.");
        } else if (ext == "FileSystem") {
            extIcon = "media-flash"
            extName = i18n("File system access");
            extDescription = i18n("Allow the app to read files stored on your device.");
        } else if (ext == "Battery") {
            extIcon = "battery-charging"
            extName = i18n("Battery");
            extDescription = i18n("Allow the app to read battery status information.");
        } else if (ext == "NetworkStatus") {
            extIcon = "network-wireless"
            extName = i18n("Network Status");
            extDescription = i18n("Allow the app to read the network status.");
        } else if (ext == "Display") {
            extIcon = "video-display"
            extName = i18n("Display");
            extDescription = i18n("Allow the app to read information about the screen and display.");
        } else if (ext == "SystemInformation") {
            extIcon = "utilities-system-monitor"
            extName = i18n("System Information");
            extDescription = i18n("Allow the app to read system information, such as device model, runtime version information and memory usage.");
        } else if (ext == "Geolocation") {
            extIcon = "marble"
            extName = i18n("Geolocation.");
            extDescription = i18n("Allow the app to read your current location.");
        } else if (ext == "Contacts") {
            extIcon = "preferences-contact-list"
            extName = i18n("Contacts");
            extDescription = i18n("Allow the app to access your addressbook and contacts.");
        } else if (ext == "Calendar") {
            extIcon = "office-calendar"
            extName = i18n("Calender");
            extDescription = i18n("Allow the app to access your calendars.");
        }
    }
}
