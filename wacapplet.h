/*
Copyright (c) 2007 Zack Rusin <zack@kde.org>
Copyright (C) 2011 Marco Martin <mart@kde.org>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */
#ifndef WACAPPLET_H
#define WACAPPLET_H

#include "webapplet.h"
#include "bundle.h"

#include "dashboardjs.h"

#include <Plasma/DataEngine>

#include <QtGui/QWidget>

class WacAppletPrivate;

class WacApplet : public WebApplet
{
    Q_OBJECT
public:
    WacApplet(QObject *parent, const QVariantList &args);
    ~WacApplet();

    bool init();

protected Q_SLOTS:
    virtual QStringList requiredExtensions() const;
    virtual void loadFinished(bool success);
    virtual void constraintsEvent(Plasma::Constraints constraints);
    virtual void initJsObjects();
    virtual void overlayLoadFinished(QDeclarativeComponent::Status s);

private:
    bool parseConfigXml();
    WacAppletPrivate* d;
};

K_EXPORT_PLASMA_APPLETSCRIPTENGINE(wac, WacApplet)

#endif
